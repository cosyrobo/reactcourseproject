import React,{Component} from 'react';
import { Card, CardImg, CardText, CardBody, CardTitle, Breadcrumb, BreadcrumbItem, Button, Modal, ModalHeader, ModalBody,
    Form, FormGroup, Input, Label, Row, Col} from 'reactstrap';
import {Link} from 'react-router-dom';
import {Control, LocalForm, Errors} from 'react-redux-form';
import {Loading} from './LoadingComponent';
import {baseUrl} from '../shared/baseUrl';
import {FadeTransform,Fade, Stagger} from 'react-animation-components';


  
  function RenderComments({comments, postComment,dishId}) {
    

    const commentsDisp = comments.map((sentence)=>{
      return(
        <Fade in>
          <ul class="list-unstyled">
            <li>{sentence.comment}</li>
            <p>      </p>
            <li>--{sentence.author} , {new Intl.DateTimeFormat('en-US', { year: 'numeric', month: 'short', day: '2-digit'}).format(new Date(Date.parse(sentence.date)))}</li>
          </ul>
        </Fade>
      )

    });


    return(
      <Card>
        <CardBody>
          <CardTitle><h4>Comments</h4></CardTitle>
          <Stagger in>
            {commentsDisp}
          </Stagger>
          <CommentForm dishId={dishId} postComment={postComment} />
        </CardBody>
      </Card>  
    );   
  }

  function RenderDish({dish}) {
      return(
        <FadeTransform in 
          transformProps ={{
            exitTransform:'scale(0.5) translateY(-50%)'
            }}>
          <Card>
            <CardImg width="100%" src={baseUrl + dish.image} alt={dish.name} />
            <CardBody>
              <CardTitle>{dish.name}</CardTitle> 
              <CardText>{dish.description}</CardText>
            </CardBody>
          </Card>
        </FadeTransform>
      );
  }

  const DishDetail = (props) => {

    if(props.isLoading){
      return(
        <div className="container">
          <div className="row">
            <Loading />
          </div>
        </div>
      );
    }
    else if (props.errMess){
      return(
        <div className="container">
          <div className="row">
            <h4>{props.errMess}</h4>
          </div>
        </div>
      );
    }
    else if (props.dish != null)
      return(
        <div className="container">
        <div className="row">
          <Breadcrumb>

            <BreadcrumbItem><Link to="/menu">Menu</Link></BreadcrumbItem>
            <BreadcrumbItem active>{props.dish.name}</BreadcrumbItem>
          </Breadcrumb>
          <div className="col-12">
            <h3>{props.dish.name}</h3>
            <hr />
          </div>                
        </div>
        <div className="row">
          <div className="col-12 col-md-5 m-1">
            <RenderDish dish={props.dish} />
          </div>
          <div className="col-12 col-md-5 m-1">
            <RenderComments comments={props.comments} 
              postComment={props.postComment}
              dishId={props.dish.id}/>
          </div>
        </div>
        </div>
      );
    else
      return(
        <div> </div>
      ); 

  }

const required = (val) => val && val.length;
const maxLength = (len) => (val) => !(val) || (val.length <= len); 
const minLength = (len) => (val) => (val) && (val.length >= len);


class CommentForm extends Component{

  constructor(props){
    super(props);
    this.state = {
      isModalOpen: false
    };

    this.toggleModal = this.toggleModal.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

  }

  toggleModal(){
    this.setState({
      isModalOpen: !this.state.isModalOpen
    });
  }


  handleSubmit(values){
        this.toggleModal();
        this.props.postComment(this.props.dishId, values.rating, values.author, values.comment);
  }

  render(){

    return(
      <React.Fragment>
      
        <Button outline onClick={this.toggleModal}>
          <span className="fa fa-pencil fa-lg"></span> Submit Comment
        </Button>


        <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
            <ModalHeader toggle={this.toggleModal}>Submit Comment</ModalHeader>
            <ModalBody>
              <LocalForm onSubmit={(values) => this.handleSubmit(values)}>
                <Row className="form-group">
                  <Col >
                    <Label htmlFor="rating">Rating</Label>
                    <Control.select model=".rating" name="rating"
                      className="form-control">
                      <option>1</option>
                      <option>2</option>
                      <option>3</option>
                      <option>4</option>
                      <option>5</option>
                    </Control.select> 
                  </Col>
                </Row>
                <Row className="form-group">
                  <Col >
                      <Label htmlFor="author">Your Name</Label>
                      <Control.text model=".author" id="author" name="author" 
                        placeholder="Your Name"
                        className ="form-control"
                        validators={{ 
                          required, minLength: minLength(3), maxLength: maxLength(15)
                        }}
                        />
                      <Errors
                        className = "text-danger"
                        model=".author"
                        show="touched"
                        messages={{
                          required: 'Required',
                          minLength: 'Must be greater than 2 characters',
                          maxLength:'Must be 15 characters or less'
                        }}
                    />
                  </Col>
                </Row>
                <Row className="form-group">
                  <Col >
                    <Label htmlFor="message">Comment</Label>             
                      <Control.textarea model=".comment" id="comment" name="comment" 
                        rows="6"  
                        className="form-control"
                        validators={{
                          required
                        }}
                        />
                  </Col>
                </Row>
                <Button type="submit" value="submit" color="primary">Submit</Button>
              </LocalForm>
            </ModalBody>
         </Modal>

      </React.Fragment>
    );
  
  }

}

    
export default DishDetail;

